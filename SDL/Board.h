#pragma once
#include <SDL.h>
#include <vector>

#include "Utils.h"
#include "Tile.h"

class Board
{
public:
    Board(const std::string& aPath);
    virtual ~Board();

    void EndGameDraw(SDL_Surface* aScreenSurface);
    void Draw(SDL_Surface* aScreenSurface);
    void ChangeImage(const std::string aString);
    bool CheckVictory(const int aX, const int aY, const Token* aToken);

    int m_CurrentTurn;
    std::vector<std::vector<Tile*>> m_BoardTiles;

private:
    int m_CurrentInARow;
    SDL_Surface* m_Surface;

    void CreateBoard();
    bool CheckHorizontalVictory(const int aX, const int aY, const Token* aToken);
    bool CheckVerticalVictory(const int aX, const int aY, const Token* aToken);
    bool CheckDiagonalVictory(const int aX, const int aY, const Token* aToken);
    bool CheckSecondDiagonalVictory(const int aX, const int aY, const Token* aToken);
};