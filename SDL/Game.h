#pragma once
#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>
#include <iostream>

#include "Board.h"
#include "Utils.h"
#include "X.h"
#include "O.h"

class Game
{
public:
    Game();
    virtual ~Game();

    //Screen dimension constants
    static const int SCREEN_WIDTH = 600;
    static const int SCREEN_HEIGHT = 600;

    static void HandleEvents(SDL_Event* e);
    static void Init();
    static void Update();
    static void Draw();
    static void Destroy();
    static bool IsRunning() { return isRunning; }

private:
    static void CreateWindow();
    static X* m_X;
    static O* m_O;
    static Board* m_Board;
    static Token* m_CurrentPlacedToken;
    static SDL_Window* m_Window;
    static SDL_Surface* m_ScreenSurface;

    static bool isDone;
    static bool isRunning;
};