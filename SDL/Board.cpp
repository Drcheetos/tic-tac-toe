#include "Board.h"

Board::Board(const std::string& aPath)
    :m_CurrentInARow(0)
{
    m_Surface = Utils::LoadPng(aPath);
    CreateBoard();
}

Board::~Board()
{
    if (m_Surface != nullptr)
    {
        SDL_FreeSurface(m_Surface);
        m_Surface = nullptr;
    }

    if (m_BoardTiles.size() > 0)
    {
        for (int i = 0; i < m_BoardTiles.size(); i++)
        {
            for (int j = 0; j < m_BoardTiles[i].size(); j++)
            {
                delete m_BoardTiles[i][j];
                m_BoardTiles[i][j] = nullptr;
            }
        }
    }
}

void Board::CreateBoard()
{
    for (int i = 0; i < 3; i++)
    {
        m_BoardTiles.push_back(std::vector<Tile*>());
        for (int j = 0; j < 3; j++)
        {
            m_BoardTiles[i].push_back(new Tile(i,j));
        }
    }
}

void Board::Draw(SDL_Surface* aScreenSurface)
{
    SDL_BlitSurface(m_Surface, nullptr, aScreenSurface, nullptr);
    for (int i = 0; i < m_BoardTiles.size(); i++)
    {
        for (int j = 0; j < m_BoardTiles[i].size(); j++)
        {
            m_BoardTiles[i][j]->Draw(aScreenSurface);
        }
    }
}

bool Board::CheckVictory(const int aX, const int aY, const Token* aToken)
{
    if (CheckHorizontalVictory(aX, aY, aToken)) 
    { 
        return true; 
    }
    else if (CheckVerticalVictory(aX, aY, aToken)) 
    { 
        return true;
    }
    else if (CheckDiagonalVictory(aX, aY, aToken)) 
    { 
        return true; 
    }
    else if (CheckSecondDiagonalVictory(aX, aY, aToken))
    {
        return true;
    }
    else 
    {
        return false;
    }
}

bool Board::CheckHorizontalVictory(const int aX, const int aY, const Token* aToken)
{
    m_CurrentInARow++;
    if (aX < 2)
    {
        if (m_BoardTiles[aX + 1][aY]->GetToken() == aToken)
        {
            m_CurrentInARow++;
            if (aX + 1 == 1)
            {
                if (m_BoardTiles[aX + 2][aY]->GetToken() == aToken)
                {
                    m_CurrentInARow++;
                }
            }
        }
    }

    if (aX > 0)
    {
        if (m_BoardTiles[aX - 1][aY]->GetToken() == aToken)
        {
            m_CurrentInARow++;
            if (aX - 1 == 1)
            {
                if (m_BoardTiles[aX - 2][aY]->GetToken() == aToken)
                {
                    m_CurrentInARow++;
                }
            }
        }
    }

    if (m_CurrentInARow == 3)
    {
        return true;
    }
    else
    {
        m_CurrentInARow = 0;
        return false;
    }
}

bool Board::CheckVerticalVictory(const int aX, const int aY, const Token* aToken)
{
    m_CurrentInARow++;
    if (aY < 2)
    {
        if (m_BoardTiles[aX][aY + 1]->GetToken() == aToken)
        {
            m_CurrentInARow++;
            if (aY + 1 == 1)
            {
                if (m_BoardTiles[aX][aY + 2]->GetToken() == aToken)
                {
                    m_CurrentInARow++;
                }
            }
        }
    }
    
    if (aY > 0)
    {
        if (m_BoardTiles[aX][aY - 1]->GetToken() == aToken)
        {
            m_CurrentInARow++;
            if (aY - 1 == 1)
            {
                if (m_BoardTiles[aX][aY - 2]->GetToken() == aToken)
                {
                    m_CurrentInARow++;
                }
            }
        }
    }

    if (m_CurrentInARow == 3)
    {
        return true;
    }
    else
    {
        m_CurrentInARow = 0;
        return false;
    }
}

bool Board::CheckDiagonalVictory(const int aX, const int aY, const Token* aToken)
{
    m_CurrentInARow++;
    if (aX == 0 && aY == 2 || aX == 1 && aY == 1)
    {
        if (m_BoardTiles[aX + 1][aY - 1]->GetToken() == aToken)
        {
            m_CurrentInARow++;
        }

        if (aX != 1)
        {
            if(m_BoardTiles[aX + 2][aY - 2]->GetToken() == aToken)
            {
                m_CurrentInARow++;
            }
        }
    }

    if (aX == 2 && aY == 0 || aX == 1 && aY == 1)
    {
        if (m_BoardTiles[aX - 1][aY + 1]->GetToken() == aToken)
        {
            m_CurrentInARow++;
        }

        if (aX != 1)
        {
            if (m_BoardTiles[aX - 2][aY + 2]->GetToken() == aToken)
            {
                m_CurrentInARow++;
            }
        }
    }

    if (m_CurrentInARow == 3)
    {
        return true;
    }
    else
    {
        m_CurrentInARow = 0;
        return false;
    }
}

bool Board::CheckSecondDiagonalVictory(const int aX, const int aY, const Token* aToken)
{
    m_CurrentInARow++;
    if (aX == 0 && aY == 0 || aX == 1 && aY == 1)
    {
        if (m_BoardTiles[aX + 1][aY + 1]->GetToken() == aToken)
        {
            m_CurrentInARow++;
        }

        if (aX != 1)
        {
            if (m_BoardTiles[aX + 2][aY + 2]->GetToken() == aToken)
            {
                m_CurrentInARow++;
            }
        }
    }

    if (aX == 2 && aY == 2 || aX == 1 && aY == 1)
    {
        if (m_BoardTiles[aX - 1][aY - 1]->GetToken() == aToken)
        {
            m_CurrentInARow++;
        }

        if (aX != 1)
        {
            if (m_BoardTiles[aX - 2][aY - 2]->GetToken() == aToken)
            {
                m_CurrentInARow++;
            }
        }
    }

    if (m_CurrentInARow == 3)
    {
        return true;
    }
    else
    {
        m_CurrentInARow = 0;
        return false;
    }
}

void Board::ChangeImage(const std::string aString)
{
    m_Surface = Utils::LoadPng(aString);
}

void Board::EndGameDraw(SDL_Surface* aScreenSurface)
{
    SDL_BlitSurface(m_Surface, nullptr, aScreenSurface, nullptr);
}