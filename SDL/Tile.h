#pragma once
#include <SDL.h>
#include "Token.h"
#include "Utils.h"

class Tile
{
public:
    Tile(const int aI, const int aJ);
    virtual ~Tile();

    void ChangePointer(Token* aToken);
    void Draw(SDL_Surface* aScreenSurface);
    Token* GetToken() { return m_Token; }

private:
    Token* m_Token;
    SDL_Rect* m_Rect;
};

