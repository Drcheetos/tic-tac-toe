#include "Token.h"



Token::Token()
{
}


Token::~Token()
{
    if (m_Surface != nullptr)
    {
        SDL_FreeSurface(m_Surface);
        m_Surface = nullptr;
    }
}

void Token::Draw(SDL_Surface* aScreenSurface, SDL_Rect* aRect)
{
    SDL_BlitSurface(m_Surface, nullptr, aScreenSurface, aRect);
}
