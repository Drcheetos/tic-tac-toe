#include "Game.h"

X* Game::m_X = nullptr;
O* Game::m_O = nullptr;
Board* Game::m_Board = nullptr; 
SDL_Window* Game::m_Window = nullptr;
SDL_Surface* Game::m_ScreenSurface = nullptr;
Token* Game::m_CurrentPlacedToken = nullptr;
bool Game::isRunning = true;
bool Game::isDone = false;

Game::Game()
{
}

Game::~Game()
{
}

void Game::Init()
{
    //Error catch message
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
    }
    else //If no error create window with board and image
    {
        CreateWindow();
        m_Board = new Board("Images\\Board.png");
        m_X = new X();
        m_O = new O();
    }
}

void Game::CreateWindow()
{
    //Create window
    m_Window = SDL_CreateWindow("Tic-Tac-Toe", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    if (m_Window == NULL)
    {
        printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
    }
    else
    {
        //Initialize PNG loading
        int imgFlags = IMG_INIT_PNG;
        if (!(IMG_Init(imgFlags) & imgFlags))
        {
            printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
        }
        else
        {
            m_ScreenSurface = SDL_GetWindowSurface(m_Window);
        }
    }
}

void Game::Update()
{
    //Event handler
    SDL_Event e;

    //Handle events on queue
    while (SDL_PollEvent(&e) != 0)
    {
        //User requests quit
        if (e.type == SDL_QUIT)
        {
            isRunning = false;
            return;
        }
        
        HandleEvents(&e);
    }
}

//Mouse events are handled here like click and drag with the exception checks
void Game::HandleEvents(SDL_Event* e)
{
    if (e->type == SDL_MOUSEMOTION || e->type == SDL_MOUSEBUTTONDOWN || e->type == SDL_MOUSEBUTTONUP)
    {
        int x;
        int y;

        SDL_GetMouseState(&x, &y);

        x = x / 200;
        y = y / 200;

        switch (e->type)
        {
        case SDL_MOUSEMOTION:
            break;

        case SDL_MOUSEBUTTONDOWN:
            if (Game::isDone == true)
            {
                Game::isRunning = false;
                break;
            }
            std::cout << "Clicked at X:  " << x << "  Y:  " << y << std::endl;
            if (m_Board->m_BoardTiles[x][y] != nullptr 
                && m_Board->m_BoardTiles[x][y]->GetToken() == nullptr)
            {
                switch (m_Board->m_CurrentTurn % 2)
                {
                    case 0:
                        std::cout << "Player 1";
                        m_Board->m_BoardTiles[x][y]->ChangePointer(m_X);
                        m_CurrentPlacedToken = m_Board->m_BoardTiles[x][y]->GetToken();
                        break;
                    default:
                        std::cout << "Player 2";
                        m_Board->m_BoardTiles[x][y]->ChangePointer(m_O);
                        m_CurrentPlacedToken = m_Board->m_BoardTiles[x][y]->GetToken();
                        break;
                }
                m_Board->m_CurrentTurn++;
            }

            if (m_Board->CheckVictory(x, y, m_CurrentPlacedToken))
            {
                std::cout << "Player " << m_CurrentPlacedToken->GetChar() << " Wins !" << std::endl;
                if (m_CurrentPlacedToken == m_X)
                {
                    m_Board->ChangeImage("Images\\XWin.png");
                    Game::isDone = true;
                }
                else if (m_CurrentPlacedToken == m_O)
                {
                    m_Board->ChangeImage("Images\\OWin.png");
                    Game::isDone = true;
                }
            }
            else if (m_Board->m_CurrentTurn == 9)
            {
                m_Board->ChangeImage("Images\\Tie.png");
                Game::isDone = true;
            }

            m_CurrentPlacedToken = nullptr;
            break;

        case SDL_MOUSEBUTTONUP:
            break;
        }
    }
}

void Game::Draw()
{
    if (!Game::isDone)
    {
        m_Board->Draw(m_ScreenSurface);
    }
    else 
    {
        m_Board->EndGameDraw(m_ScreenSurface);
    }

    // Update full window
    SDL_UpdateWindowSurface(m_Window);
}

void Game::Destroy()
{
    if (m_ScreenSurface != nullptr)
    {
        SDL_FreeSurface(m_ScreenSurface);
        m_ScreenSurface = nullptr;
    }

    if (m_CurrentPlacedToken != nullptr)
    {
        delete m_CurrentPlacedToken;
        m_CurrentPlacedToken = nullptr;
    }

    if (m_Board != nullptr)
    {
        delete m_Board;
        m_Board = nullptr;
    }

    if (m_Window != nullptr)
    {
        SDL_DestroyWindow(m_Window);
        m_Window = nullptr;
    }

    IMG_Quit();
    SDL_Quit();
}