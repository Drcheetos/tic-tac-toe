/*This source code copyrighted by Lazy Foo' Productions (2004-2019)
and may not be redistributed without written permission.*/

//Using SDL, SDL_image, standard IO, and strings
#include "Game.h"
#include <string>

int main(int argc, char* args[])
{
    Game::Init();

    while (Game::IsRunning())
    {
        Game::Update();
        Game::Draw();
    }

    Game::Destroy();

    return 0;
}