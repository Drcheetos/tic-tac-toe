#pragma once
#include <SDL.h>
#include <string>
#include "Utils.h"

class Token
{
public:
    Token();
    virtual ~Token();

    void Draw(SDL_Surface* aScreenSurface, SDL_Rect* aRect);
    const char* GetChar() { return &m_Char; }

protected:
    char m_Char;
    SDL_Surface* m_Surface;
};

