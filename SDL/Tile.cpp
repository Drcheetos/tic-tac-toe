#include "Tile.h"


Tile::Tile(const int aI, const int aJ)
{
    m_Token = nullptr;
    m_Rect = Utils::CreateRect(aI, aJ);
}


Tile::~Tile()
{
    if (m_Rect != nullptr)
    {
        delete m_Rect;
        m_Rect = nullptr;
    }

    if (m_Token != nullptr)
    {
        m_Token = nullptr;
    }
}

void Tile::ChangePointer(Token* aToken)
{
    m_Token = aToken;
}

void Tile::Draw(SDL_Surface* aScreenSurface)
{
    if (m_Token != nullptr)
    {
        m_Token->Draw(aScreenSurface, m_Rect);
    }
}
