#include "Utils.h"

Utils::Utils()
{
}


Utils::~Utils()
{
}

SDL_Surface* Utils::LoadPng(const std::string& aPath)
{
    //Load image at specified path
    SDL_Surface* loadedSurface = IMG_Load(aPath.c_str());
    if (loadedSurface == NULL)
    {
        printf("Unable to load image %s! SDL_image Error: %s\n", IMG_GetError());
    }
    else
    {
        return loadedSurface;
    }

    return nullptr;
}

SDL_Rect* Utils::CreateRect(const int aI, const int aJ)
{
    SDL_Rect* rect = new SDL_Rect();
    rect->x = aI * 200;
    rect->y = aJ * 200;
    rect->h = 200;
    rect->w = 200;

    return rect;
}