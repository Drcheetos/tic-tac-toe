#pragma once
#include <SDL.h>
#include <string>
#include <SDL_image.h>

class Utils
{
public:
    Utils();
    virtual ~Utils();

    static SDL_Surface* LoadPng(const std::string& aPath);

    static SDL_Rect* CreateRect(const int aI, const int aJ);

};